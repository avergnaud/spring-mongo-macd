package hello.persistence;

import hello.model.Macd;

public interface MacdRepositoryCustom {

    public boolean upsert(Macd newMacd);
}
