package hello.persistence;

import hello.model.Macd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.math.RoundingMode;

public class MacdRepositoryImpl implements MacdRepositoryCustom {

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * update if {pair, interval, timestamp} exists
     * or insert
     * @param macd
     * @return
     */
    @Override
    public boolean upsert(Macd macd) {

        MongoOperations mongoOperation = (MongoOperations) mongoTemplate;

        Query query = new Query();
        query.addCriteria(Criteria.where("pair").is(macd.getPair())
            .and("interval").is(macd.getInterval())
            .and("timestamp").is(macd.getTimestamp()));

        Update update = new Update();
        if(macd.getClose() != null) {
            update.set("close", macd.getClose().setScale(10, RoundingMode.HALF_EVEN));
        }
        update.set("shortPeriodNumber", macd.getShortPeriodNumber());
        update.set("longPeriodNumber", macd.getLongPeriodNumber());
        update.set("signalPeriodNumber", macd.getSignalPeriodNumber());
        if(macd.getEmaShort() != null) {
            update.set("emaShort", macd.getEmaShort().setScale(10, RoundingMode.HALF_EVEN));
        }
        if(macd.getEmaLong() != null) {
            update.set("emaLong", macd.getEmaLong().setScale(10, RoundingMode.HALF_EVEN));
        }
        if(macd.getMacdValue() != null) {
            update.set("macdValue", macd.getMacdValue().setScale(10, RoundingMode.HALF_EVEN));
        }
        if(macd.getSignal() != null) {
            update.set("signal", macd.getSignal().setScale(10, RoundingMode.HALF_EVEN));
        }

        mongoOperation.upsert(query, update, Macd.class);

        return true;
    }
}
