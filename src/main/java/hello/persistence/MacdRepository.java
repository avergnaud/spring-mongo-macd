package hello.persistence;

import hello.model.Macd;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@RepositoryRestResource(collectionResourceRel = "macd", path = "macd")
public interface MacdRepository extends MongoRepository<Macd, String>, MacdRepositoryCustom {
}
