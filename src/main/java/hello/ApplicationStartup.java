package hello;

import hello.financial.MacdCalculator;
import hello.http.KrakenPublicRequest;
import hello.model.Macd;
import hello.persistence.MacdRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import javax.json.JsonObject;
import javax.json.JsonArray;
import java.math.BigDecimal;

@Component
public class ApplicationStartup
        implements ApplicationListener<ApplicationReadyEvent> {

    // TODO tmp
    public static final String PAIR = "XETHZEUR";
    public static final int INTERVAL = 240;

    private static final Logger log = LoggerFactory.getLogger(ApplicationStartup.class);

    @Autowired
    MacdRepository macdRepository;

    @Autowired
    MacdCalculator macdCalculator;

    /**
     * This event is executed as late as conceivably possible to indicate that
     * the application is ready to service requests.
     */
    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {

        JsonObject jsonObject = new KrakenPublicRequest().queryOHLC();

        JsonObject result = (JsonObject) jsonObject.get("result");

        JsonArray pairInfo = (JsonArray) result.get(PAIR);

        for (int i = 0; i < pairInfo.size(); i++) {
            JsonArray ohlcFromKraken = pairInfo.getJsonArray(i);
            Macd macd = new Macd();
            macd.setPair(PAIR);
            macd.setInterval(INTERVAL);
            macd.setTimestamp(ohlcFromKraken.getInt(0));
            macd.setClose(new BigDecimal(ohlcFromKraken.getString(4)));
            macdRepository.upsert(macd);
        }

        macdCalculator.updateAll();

        return;
    }

} // class
