package hello.model;

import org.springframework.data.annotation.Id;

import java.math.BigDecimal;

public class Macd {
    @Id
    private String id;

    private String pair;
    private int interval;
    private int timestamp;
    private BigDecimal close;
    private int shortPeriodNumber;/* 12 */
    private int longPeriodNumber;/* 26 */
    private int signalPeriodNumber;/* 9 */
    private BigDecimal emaShort;
    private BigDecimal emaLong;
    private BigDecimal macdValue;
    private BigDecimal signal;

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    public BigDecimal getClose() {
        return close;
    }

    public void setClose(BigDecimal close) {
        this.close = close;
    }

    public int getShortPeriodNumber() {
        return shortPeriodNumber;
    }

    public void setShortPeriodNumber(int shortPeriodNumber) {
        this.shortPeriodNumber = shortPeriodNumber;
    }

    public int getLongPeriodNumber() {
        return longPeriodNumber;
    }

    public void setLongPeriodNumber(int longPeriodNumber) {
        this.longPeriodNumber = longPeriodNumber;
    }

    public int getSignalPeriodNumber() {
        return signalPeriodNumber;
    }

    public void setSignalPeriodNumber(int signalPeriodNumber) {
        this.signalPeriodNumber = signalPeriodNumber;
    }

    public BigDecimal getEmaShort() {
        return emaShort;
    }

    public void setEmaShort(BigDecimal emaShort) {
        this.emaShort = emaShort;
    }

    public BigDecimal getEmaLong() {
        return emaLong;
    }

    public void setEmaLong(BigDecimal emaLong) {
        this.emaLong = emaLong;
    }

    public BigDecimal getMacdValue() {
        return macdValue;
    }

    public void setMacdValue(BigDecimal macdValue) {
        this.macdValue = macdValue;
    }

    public BigDecimal getSignal() {
        return signal;
    }

    public void setSignal(BigDecimal signal) {
        this.signal = signal;
    }
}
