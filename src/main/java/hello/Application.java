package hello;

import hello.financial.MacdCalculator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

    @Bean
    public MacdCalculator macdCalculator() {
        return new MacdCalculator(12,26,9);
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}