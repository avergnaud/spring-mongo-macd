package hello.http;

import hello.ApplicationStartup;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.net.ssl.HttpsURLConnection;

/*	
TODO...
 */

public class KrakenPublicRequest {

    public JsonObject queryOHLC() {

        String address = "https://api.kraken.com/0/public/OHLC?pair=" + ApplicationStartup.PAIR
                + "&interval=" + ApplicationStartup.INTERVAL;

        URL url = null;
        HttpsURLConnection con = null;
        try {
            url = new URL(address);
            con = (HttpsURLConnection) url.openConnection();

            con.setConnectTimeout(5_000);//5 secondess

            con.setRequestMethod("GET");
        } catch (IOException e) {
            e.printStackTrace();
        }

        StringBuilder response = new StringBuilder();
        try (InputStream inputStream = con.getInputStream();
             BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));) {
            String inputLine = null;
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        String result = response.toString();

        JsonObject jsonObject = null;
        try(JsonReader reader = Json.createReader(new StringReader(result))) {
            jsonObject = reader.readObject();

        }
        return jsonObject;
    }
}
