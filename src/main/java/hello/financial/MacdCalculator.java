package hello.financial;

import hello.model.Macd;
import hello.persistence.MacdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

@Service
public class MacdCalculator {

    @Autowired
    MacdRepository macdRepository;

    private int shortPeriodNumber;
    private int longPeriodNumber;
    private int signalPeriodNumber;

    /*
    k = 2 / (nbPeriods + 1)
    currentClose * k + previousEma * (1 - k)
     */
    private static BigDecimal ema(BigDecimal currentClose, BigDecimal previousEma, BigDecimal nbPeriods) {
        BigDecimal k = BigDecimal.valueOf(2).divide(nbPeriods.add(BigDecimal.valueOf(1)), 10, RoundingMode.HALF_EVEN);
        return currentClose.multiply(k).add(previousEma.multiply(BigDecimal.valueOf(1).subtract(k)));
    }

    /* 12, 26, 9 */
    public MacdCalculator(int shortPeriodNumber, int longPeriodNumber, int signalPeriodNumber) {
        this.shortPeriodNumber = shortPeriodNumber;
        this.longPeriodNumber = longPeriodNumber;
        this.signalPeriodNumber = signalPeriodNumber;
    }

    public void updateAll() {

        /* création d'une Map pour faciliter les calculs */
        List<Macd> macdList = macdRepository.findAll(Sort.by(Sort.Direction.ASC, "timestamp"));
        SortedMap<Integer, Macd> macdMap = new TreeMap<>();
        int i = 0;
        for (Macd macd : macdList) {
            macdMap.put(Integer.valueOf(i++), macd);
        }

        /* emaShort 12 */
        BigDecimal sommeInitiale = BigDecimal.valueOf(0);
        for (Map.Entry<Integer, Macd> macdById : macdMap.entrySet()) {
            BigDecimal emaShort;
            if (macdById.getKey() < this.shortPeriodNumber - 1) {
                sommeInitiale = sommeInitiale.add(macdById.getValue().getClose());
                continue;
            } else if (macdById.getKey() == this.shortPeriodNumber - 1) {
                sommeInitiale = sommeInitiale.add(macdById.getValue().getClose());
                emaShort = sommeInitiale.divide(BigDecimal.valueOf(this.shortPeriodNumber), 10, RoundingMode.HALF_EVEN);
            } else {
                emaShort = ema(macdById.getValue().getClose(), macdMap.get(macdById.getKey() - 1).getEmaShort(), BigDecimal.valueOf(this.shortPeriodNumber));
            }
            macdById.getValue().setShortPeriodNumber(this.shortPeriodNumber);
            macdById.getValue().setEmaShort(emaShort);
            macdRepository.upsert(macdById.getValue());
        }

        /* emaLong 26, macdValue */
        sommeInitiale = BigDecimal.valueOf(0);
        for(Map.Entry<Integer, Macd> macdById : macdMap.entrySet()) {
            BigDecimal emaLong;
            BigDecimal macdValue;
            if(macdById.getKey() < this.longPeriodNumber - 1) {
                sommeInitiale = sommeInitiale.add(macdById.getValue().getClose());
                continue;
            } else if(macdById.getKey() == this.longPeriodNumber - 1) {
                sommeInitiale = sommeInitiale.add(macdById.getValue().getClose());
                emaLong = sommeInitiale.divide(BigDecimal.valueOf(this.longPeriodNumber), 10, RoundingMode.HALF_EVEN);
                macdValue = macdById.getValue().getEmaShort().subtract(emaLong);
            } else {
                emaLong = ema(macdById.getValue().getClose(), macdMap.get(macdById.getKey() -1).getEmaLong(), BigDecimal.valueOf(this.longPeriodNumber));
                macdValue = macdById.getValue().getEmaShort().subtract(emaLong);
            }
            macdById.getValue().setLongPeriodNumber(this.longPeriodNumber);
            macdById.getValue().setEmaLong(emaLong);
            macdById.getValue().setMacdValue(macdValue);
            macdRepository.upsert(macdById.getValue());
        }

        /* signal */
        for(Map.Entry<Integer,Macd> macdById : macdMap.entrySet()) {
            if(macdById.getKey() >= (this.longPeriodNumber - 1) + (this.signalPeriodNumber - 1)) {
                BigDecimal somme = BigDecimal.valueOf(0);
                for(int j = macdById.getKey() - this.signalPeriodNumber + 1; j<= macdById.getKey(); j++) {
                    somme = somme.add(macdMap.get(j).getMacdValue());
                }
                BigDecimal moyenne = somme.divide(BigDecimal.valueOf(this.signalPeriodNumber), 10, RoundingMode.HALF_EVEN);
                macdById.getValue().setSignalPeriodNumber(this.signalPeriodNumber);
                macdById.getValue().setSignal(moyenne);
                macdRepository.upsert(macdById.getValue());
            }
        }
    }
}
