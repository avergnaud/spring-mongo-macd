# hors container

mvn -N io.takari:maven:0.7.5:wrapper

./mvnw spring-boot:run

# Docker

docker-compose up

# endpoint

http://localhost:8080/macd

http://localhost:8080/macd/?page=35

# mongo

use spring-macd

db.macd.find().sort({timestamp : -1})

db.macd.find({ "timestamp" : 1545321600 })

# suite

https://docs.gitlab.com/ee/user/project/container_registry.html#build-and-push-images-using-gitlab-ci